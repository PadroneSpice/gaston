/* Gaston by Sean Castillo */
int originX      = 1192;
int originY      = 104;
int FRAMECOUNT   = 15;
int currentFrame = 0;
PImage[] eggCannon1 = new PImage[FRAMECOUNT];
PImage[] eggCannon2 = new PImage[FRAMECOUNT];
PImage[] eggCannon3 = new PImage[FRAMECOUNT];
PImage cake;
PImage eggJoint;
PImage eggJoint2;
PImage[] arm1                 = new PImage[5];
PImage[] arm2                 = new PImage[6];
PImage[] headAnimation        = new PImage[6];
PImage[] raspberryPiAnimation = new PImage[4];
PImage[] hdmiAnimation        = new PImage[8];
PImage neck;
PImage signature;

boolean headMovementSwitch = true;
int     moveHeadCount      = 0;
int     moveHeadDirectionY = 0;
int     moveHeadDirectionX = 0;
int     moveBackCount      = 0;

boolean piMovementSwitch = true;
int     movePiDirectionY = 0;

boolean rightJointMovementSwitch = true;
int moveRightJointCount          = 0;
int moveRightJointDirectionX     = 0;

boolean leftJointMovementSwitch = true;
int     moveLeftJointCount      = 0;
int     moveLeftJointDirectionX = 0;

boolean mouthOpen = false;

int Red   = 89;
int Green = 76;
int Blue  = 112;

void setup()
{
    frameRate(14);
    //size(2384, 1828);
    size(2384, 1700);
    noStroke();
    smooth();
    
    //signature
    signature = loadImage("Signature_Sean_Hydrone.png");

    //neck
    neck = loadImage("neckAnimFrame4.png");

    //cake
    cake = loadImage("cake.png");
    
    //eggCannon1 frames
    for (int i=0; i<10; i++)
    {
        String eggCannon1Frame = "EggTest2_frame" + nf(i,1) + ".png";
        eggCannon1[i] = loadImage(eggCannon1Frame);
        eggCannon2[i] = loadImage(eggCannon1Frame);
        eggCannon3[i] = loadImage(eggCannon1Frame);
    }
    for (int i=10; i<15; i++)
    {
       eggCannon1[i] = loadImage("EggTest2_frame9.png");
       eggCannon2[i] = loadImage("EggTest2_frame9.png");
       eggCannon3[i] = loadImage("EggTest2_frame9.png");
    }
     
    //egg joints
    eggJoint = loadImage("EggTest2_frame0.png");
    eggJoint2 = loadImage("otherEgg2.png");
    
    for (int i=0; i<5; i++)
    {
        String arm1Frame = "arm1AnimTest1frame" + nf(i,1) + ".png";
        arm1[i] = loadImage(arm1Frame);
    }
    
    //other arm frames
    for (int i=0; i<6; i++)
    {
        String arm2Frame = "Arm2Anim_frame" + nf(i,1) + ".png";
        arm2[i] = loadImage(arm2Frame);
    }
    
    //head animation frames
    for (int i=0; i<6; i++)
    {
       String  GastonHeadFrame = "Gaston_Head_frame" + nf(i,1) + ".png";
       headAnimation[i] = loadImage(GastonHeadFrame);
    }
    
    
    //raspberry pi animation frames
    for (int i=0; i<4; i++)
    {
       String PiFrame = "PiAnimFrame_" + nf(i,1) + ".png";
       raspberryPiAnimation[i] = loadImage(PiFrame);
    }
    
    //hdmi cable animation frames
    for (int i=0; i<8; i++)
    {
       String hdmiFrame = "hdmi_anim_frame" + nf(i, 1) + ".png";
       hdmiAnimation[i] = loadImage(hdmiFrame);
    }
    
    //resizing (i.e. shrinking) images
    signature.resize(200, 0);
    cake.resize(500, 0);
    eggJoint.resize(400, 0);
    eggJoint2.resize(200, 0);
    neck.resize(900, 0);
    for (int i=0; i<FRAMECOUNT; i++)
    {
      eggCannon1[i].resize(400, 0);
      eggCannon2[i].resize(400, 0);
      eggCannon3[i].resize(400, 0);
    }
    for (int i=0; i<5; i++)
    {
      arm1[i].resize(400, 0);
    }
    for (int i=0; i<6; i++)
    {
      arm2[i].resize(550, 0);
    }
    for(int i=0; i<6; i++)
    {
      headAnimation[i].resize(700, 0); 
    }
    for(int i=0; i<4; i++)
    {
      raspberryPiAnimation[i].resize(320, 0); 
    }
    for (int i=0; i<8; i++)
    {
      hdmiAnimation[i].resize(900, 0); 
    }
}

void draw()
{
    background(Red, Green, Blue);
    currentFrame = (currentFrame + 1) % FRAMECOUNT;

    //display neck
    image(neck, originX-550, originY-70);

    //display cake
    image(cake, 400+movePiDirectionY, 350+movePiDirectionY);
    
    //display egg joints
    moveLeftJointCount++;
    if (moveLeftJointCount == 10)
    {
      leftJointMovementSwitch = !leftJointMovementSwitch;
      moveLeftJointCount = 0;
    }
    if (leftJointMovementSwitch == true)
    {
       moveLeftJointDirectionX -= 5; 
    }
    else
    {
       moveLeftJointDirectionX += 5; 
    }
    
    image(eggJoint, 270+moveLeftJointDirectionX, 480);
    
    moveRightJointCount++;
    if (moveRightJointCount == 10)
    {
      rightJointMovementSwitch = !rightJointMovementSwitch; 
      moveRightJointCount = 0;
    }
    if (rightJointMovementSwitch == true)
    {
       moveRightJointDirectionX += 2; 
    }
    else
    {
       moveRightJointDirectionX -= 2; 
    }
    image(eggJoint2, 700+moveRightJointDirectionX, 500);
    
    //display arms
    image(arm1[(currentFrame) % 5], 150+moveLeftJointDirectionX, 540);

    //display arm2
    image(arm2[(currentFrame) % 6], 800+moveRightJointDirectionX, 500); 
    

    //display head animation
    //goal: goes up 20 units, then goes down 20 units
    moveHeadCount++;
    if (moveHeadCount == 20)
    {
       headMovementSwitch = !headMovementSwitch; //switches the direction
       moveHeadCount = 0;                        //resets the moveHeadCount
    }
    //movement
    if (headMovementSwitch == true)
    {
       //move head in Y direction
       moveHeadDirectionY += 2;
       //move head in X direction
       moveHeadDirectionX += 4;
    }
    else
    {
       //move head in Y direction
       moveHeadDirectionY -= 2; 
       //move head in X direction
       moveHeadDirectionX -= 4;
    }
    
    image(headAnimation[(currentFrame) % 6], originX-420+moveHeadDirectionX, originY-50-moveHeadDirectionY);

    //back of Gaston
    //movement pattern
    moveBackCount++;

    if (moveBackCount == 10)
    {
       piMovementSwitch = !piMovementSwitch;
       moveBackCount = 0;
    }
    if (piMovementSwitch == true)
    {
      movePiDirectionY += 2;
    }
    else
    {
      movePiDirectionY -= 2; 
    }
        
    image(raspberryPiAnimation[3], originX-980, originY+90+movePiDirectionY);
    image(raspberryPiAnimation[2], originX-870, originY+130+movePiDirectionY);
    image(raspberryPiAnimation[1], originX-780, originY+150+movePiDirectionY);
    image(raspberryPiAnimation[0], originX-650, originY+180+movePiDirectionY);
    
    //display hdmi animation
    if ( currentFrame % FRAMECOUNT >=10)
    {
       mouthOpen = true; 
    }
    else
    {
       mouthOpen = false; 
    }
    //display egg cannons
    if (mouthOpen == true) {image(hdmiAnimation[(currentFrame) % 5], 520+eggCannon1[(currentFrame) % FRAMECOUNT].width/2+moveHeadDirectionX, 110+eggCannon1[(currentFrame) % FRAMECOUNT].height/2 - moveHeadDirectionY);}
    image(eggCannon1[(currentFrame) % FRAMECOUNT], originX+200+moveHeadDirectionX, originY+110-moveHeadDirectionY);
    if (mouthOpen == true) {image(hdmiAnimation[(currentFrame) % 5], 460+eggCannon2[(currentFrame) % FRAMECOUNT].width/2-moveHeadDirectionX, 160+eggCannon2[(currentFrame) % FRAMECOUNT].height/2 + moveHeadDirectionY);}
    image(eggCannon2[(currentFrame) % FRAMECOUNT], originX+220-moveHeadDirectionX, originY+160+moveHeadDirectionY);
    if (mouthOpen == true) {image(hdmiAnimation[(currentFrame) % 5], 500+eggCannon3[(currentFrame) % FRAMECOUNT].width/2+moveHeadDirectionX, 180+eggCannon3[(currentFrame) % FRAMECOUNT].height/2 + moveHeadDirectionY);}
    image(eggCannon3[(currentFrame) % FRAMECOUNT], originX+200+moveHeadDirectionX, originY+180+moveHeadDirectionY);
    
    //display signature
    image(signature, 0, 10);
}
